package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

	private final Map<String, Integer> albums = new HashMap<>();
	private final Set<Song> songs = new HashSet<>();

	@Override
	public void addAlbum(final String albumName, final int year) {
		this.albums.put(albumName, year);
	}

	@Override
	public void addSong(final String songName, final Optional<String> albumName, 
			final double duration) {
		if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
			throw new IllegalArgumentException("invalid album name");
		}
		this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
	}

	@Override
	public Stream<String> orderedSongNames() {
		return songs.stream()
				.map(s -> s.getSongName())
				.sorted(); 
	}

	@Override
	public Stream<String> albumNames() {
		return albums.keySet().stream()
				.sorted();
	}

	@Override
	public Stream<String> albumInYear(final int year) {
		return albums.entrySet().stream()
				.filter(entry -> entry.getValue().equals(year))
				.map(entry -> entry.getKey());
	}

	@Override
	public int countSongs(final String albumName) {
		return (int) songs.stream()
				.filter(song -> song.getAlbumName().equals(
						Optional.of(albumName)))
				.count();
	}

	@Override
	public int countSongsInNoAlbum() {
		return (int) songs.stream()
				.filter(song -> !song.getAlbumName().isPresent())
				.count();
	}

	@Override
	public OptionalDouble averageDurationOfSongs(final String albumName) {
		return songs.stream()
				.filter(song -> song.getAlbumName().equals(Optional.of(albumName)))
				.mapToDouble(song -> song.getDuration())
				.average();
	}

	@Override
	public Optional<String> longestSong() {
		return Optional.of(songs.stream()
								.max((song1, song2) -> Double.compare(
										song1.getDuration(), song2.getDuration()))
								.get()
								.getSongName());
	}

	@Override
	public Optional<String> longestAlbum() {
		/* Creates a map with album names as keys and the corresponding
		 * durations as values */
		Map<Optional<String>, Double> lengthsMap = new HashMap<>();
		songs.stream()
		.collect(Collectors.groupingBy(Song::getAlbumName))
		.forEach((albumName, albumSongs) -> {
			Double length = albumSongs.stream()
					.mapToDouble(Song::getDuration)
					.sum();
			lengthsMap.put(albumName, length);
		});
		/* Returns the key of the entry with maximum duration */
		return lengthsMap.entrySet()
				.stream()
				.max((entry1, entry2) -> Double.compare(
						entry1.getValue(), entry2.getValue()))
				.get().getKey();
	}

	private static final class Song {

		private final String songName;
		private final Optional<String> albumName;
		private final double duration;
		private int hash;

		Song(final String name, final Optional<String> album, final double len) {
			super();
			this.songName = name;
			this.albumName = album;
			this.duration = len;
		}

		public String getSongName() {
			return songName;
		}

		public Optional<String> getAlbumName() {
			return albumName;
		}

		public double getDuration() {
			return duration;
		}

		@Override
		public int hashCode() {
			if (hash == 0) {
				hash = songName.hashCode() ^ albumName.hashCode() 
						^ Double.hashCode(duration);
			}
			return hash;
		}

		@Override
		public boolean equals(final Object obj) {
			if (obj instanceof Song) {
				final Song other = (Song) obj;
				return albumName.equals(other.albumName) 
						&& songName.equals(other.songName)
						&& duration == other.duration;
			}
			return false;
		}

		@Override
		public String toString() {
			return "Song [songName=" + songName 
					+ ", albumName=" + albumName 
					+ ", duration=" + duration + "]";
		}

	}

}
